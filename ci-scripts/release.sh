#!/usr/bin/env bash

set -ex
IMAGE=$1

##
# Step 1: Generate new version
##
previous_version=$(semversioner current-version)
semversioner release || true
new_version=$(semversioner current-version)

##
# Step 2: Verify Branch
##

if [[ "${BITBUCKET_BRANCH}" == "master" ]]; then
    tags=("${new_version}")
elif [[ "${BITBUCKET_BRANCH}" =~ "feature-" ]]; then
    tags=("${new_version}-feature-${BITBUCKET_BUILD_NUMBER}")
else
    echo "ERROR: Can only push docker images from master or feature- branches."
    exit 1
fi

##
# Step 3: Build and push docker image
##
echo "Docker login..."
echo ${REGISTRY_PASSWORD} | docker login --username "$REGISTRY_USERNAME" --password-stdin

echo "Build and push docker image..."
docker build -t ${IMAGE}:pipelines-${BITBUCKET_BUILD_NUMBER} .
for tag in "${tags[@]}"; do
    docker tag "${IMAGE}:pipelines-${BITBUCKET_BUILD_NUMBER}" "${IMAGE}:${tag}"
    docker push "${IMAGE}:${tag}"
done

##
# Step 4: Determine if there were changes. 
##
if [[ "${previous_version}" == "${new_version}" ]]; then
    echo "No changes to report in CHANGELOG"  
    echo "Moving the repo tag ${new_version}"
    git push origin :refs/tags/${new_version}
    git tag -fa ${new_version} -m "Add ${new_version} tag" 
    git push -f origin ${new_version}
else
    ##
    # Generate CHANGELOG.md
    ##
    echo "Generating CHANGELOG.md file..."
    semversioner changelog > CHANGELOG.md
    # Use new version in the README.md examples
    echo "Replace version '$previous_version' to '$new_version' in README.md ..."
    sed -i "s/$BITBUCKET_REPO_SLUG:[0-9]*\.[0-9]*\.[0-9]*/$BITBUCKET_REPO_SLUG:$new_version/g" README.md
    # Use new version in the pipe.yml metadata file
    echo "Replace version '$previous_version' to '$new_version' in pipe.yml ..."
    sed -i "s/$BITBUCKET_REPO_SLUG:[0-9]*\.[0-9]*\.[0-9]*/$BITBUCKET_REPO_SLUG:$new_version/g" pipe.yml

    ##
    # Commit back to the repository
    ##
    echo "Committing updated files to the repository..."
    git add .
    git commit -m "Update files for new version '${new_version}' [skip ci]"
    git push origin ${BITBUCKET_BRANCH}

    ##
    # Tag the repository
    ##

    echo "Tagging for release ${new_version}" "${new_version}"
    git tag -a -m "Tagging for release ${new_version}" "${new_version}"
    git push origin ${new_version}

fi
