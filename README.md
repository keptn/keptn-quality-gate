# Bitbucket Pipelines Pipe: Keptn Quality Gate

This pipe show how easy it is to integrate SLI/SLO-based [Keptn](https://keptn.sh) Quality Gates to your pipeline. Use cases:

1. Run a `performance test` and then call the Keptn Quality Gate to evaluate whether service levels were met
1. Run a `deployment`, such canary or blue/green, and then call the Keptn Quality Gate to evaluate whether service levels are maintained

See an example pipeline using this pipe in [this bitbucket repo](https://bitbucket.org/dynatracedemos/simplenodeservice)

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: keptn/keptn-quality-gate:0.1.0
  variables:
    KEPTN_URL: "<string>"
    KEPTN_TOKEN: "<string>"
    START: "<string>"
    END: "<string>"
    PROJECT: "<string>"
    SERVICE: "<string>"
    STAGE: "<string>"
    LABELS: "<string>"
    DEBUG: "<string>"
    PROCESS_STATUS: "<string>"
```
## Variables

| Variable | Usage |
| --- | --- |
| KEPTN_URL (*)   | Keptn Base URL|
| KEPTN_TOKEN (*) | Keptn API Token|
| START (*)       | Evaluation start time in ISO format |
| END (*)         | Evaluation end time in ISO format |
| PROJECT (*)     | Keptn project name to evaluate |
| SERVICE (*)     | Keptn service name to evaluate  |
| STAGE (*)       | Keptn stage name to evaluate  |
| LABELS          | Labels that will show in the Keptn Bridge |
| DEBUG           | Turn on extra debug information. Default: `false`. |
| PROCESS_STATUS  | Evaluate the status it stop the pipeline. Default: `ignore`. |

_(*) = required variable._

## Details

1. KEPTN_URL and KEPTN_TOKEN pipe variables

    Get your Keptn URL and Token values from these [Keptn instructions](https://keptn.sh/docs/0.6.0/reference/cli/#keptn-auth)

    Recommend to store these a pipeline variables with the KEPTN_TOKEN as a locked variable.

1. START and END pipe variables

    The expected format is `2019-11-21T11:00:00Z`. You can use this command in the pipeline.

    ```
    date -u "+%Y-%m-%dT%H:%M:%SZ"
    ```

1. PROCESS_STATUS pipe variables

    Valid values are:

    * ```pass_on_warning``` - a `fail` keptn result status will stop the pipeline
    * ```fail_on_warning```- a `fail` or `warning` keptn result status will stop the pipeline
    * ```ignore``` - will not process the keptn status

1. LABELS pipe variables

This an set of label key-value pairs that will show on the Keptn Bridge. For this example: `LABELS: '{"source":"bitbucket","build":"${BITBUCKET_BUILD_NUMBER}"}'`

1. quality-gate-result file

    This pipe will also write out a file called ```quality-gate-result.json``` into the root folder of the pipeline. This file can be parsed for custom logging or status processing.  See example in the next example section below.

## Prerequisites

1. Kubernetes Cluster with Keptn Quality Gate services installed. Tested with Keptn 0.6.0
2. Project & service onboarded to Keptn with the required SLIs & SLOs resource files

_See [keptn docs](https://keptn.sh/docs/0.6.0/usecases/quality-gates/) for details_

## Examples

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file.

This example shows the pipe performing the evaluations of the result.  *(note the PROCESS_STATUS setting)*

```yaml
- step: &qualitygate
    name: Call Keptn Quality Gate
    script:
        - TEST_START=$(date -u "+%Y-%m-%dT%H:%M:%SZ")
        - ./runloadtest.sh
        - TEST_END=$(date -u "+%Y-%m-%dT%H:%M:%SZ")
        - pipe: keptn/keptn-quality-gate:0.1.0
          variables:
            KEPTN_URL: "$KEPTN_API_URL"
            KEPTN_TOKEN: "$KEPTN_TOKEN"
            START: "$TEST_START"
            END: "$TEST_END"
            PROJECT: "bitbucket-demo"
            SERVICE: "simplenodeservice"
            STAGE: "development"
            LABELS: '{"source":"bitbucket","build":"${BITBUCKET_BUILD_NUMBER}"}'
            DEBUG: "true"
            PROCESS_STATUS: "pass_on_warning"
```

This example skips the pipe evaluation of the result and also shows the secondary step to parse of the ```quality-gate-result.json``` file that is generated.  *(note PROCESS_STATUS is left out and defaults to ignore)*

```yaml
- step: &qualitygate
    name: Call Keptn Quality Gate
    script:
        - TEST_START=$(date -u "+%Y-%m-%dT%H:%M:%SZ")
        - ./runloadtest.sh
        - TEST_END=$(date -u "+%Y-%m-%dT%H:%M:%SZ")
        - pipe: keptn/keptn-quality-gate:0.1.0
          variables:
            KEPTN_URL: "$KEPTN_API_URL"
            KEPTN_TOKEN: "$KEPTN_TOKEN"
            START: "$TEST_START"
            END: "$TEST_END"
            PROJECT: "bitbucket-demo"
            SERVICE: "simplenodeservice"
            STAGE: "development"
            LABELS: '{"source":"bitbucket","build":"${BITBUCKET_BUILD_NUMBER}"}'
            DEBUG: "true"

    artifacts:
      - quality-gate-result

- step: &inspect_result
    script:
      - STATUS=$(cat quality-gate-result.json|jq -r ".data.evaluationdetails.result")
      - SCORE=$(cat quality-gate-result.json|jq -r ".data.evaluationdetails.score")
      - echo "STATUS = $STATUS"
      - echo "SCORE = $SCORE"
      - echo "Full file contents"
      - cat quality-gate-result.json 

```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know. The pipe is maintained by Dynatrace. You can contact us directly at integrations@dynatrace.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
