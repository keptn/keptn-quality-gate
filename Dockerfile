FROM alpine:3.10.3

LABEL description="Call Keptn Quality Gate"
LABEL repository="https://bitbucket.org/keptn/keptn-quality-gate"

RUN apk --no-cache add \
    bash \
    jq

RUN apk add curl

ADD https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.6.0/common.sh /
COPY pipe /
COPY LICENSE README.md pipe.yml /

WORKDIR /opt/atlassian/bitbucketci/agent/build
ENTRYPOINT ["/pipe.sh"]
