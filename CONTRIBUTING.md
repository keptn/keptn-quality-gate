# Contributing to Bitbucket Pipes

Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

# Release process

The `bitbucket-pipelines.yml` BitBucket pipeline automates the process build and push the pipe to a registry.  

Image versions are increased automatically using semantic versioning using a tool called [`semversioner`](https://pypi.org/project/semversioner/). 

Once pushed, the pipeline will call `ci-scripts/release.sh`.  This script will:

- run the test suite defined in the `test/` subfolder _(Assuming test step in The `bitbucket-pipelines.yml` is uncommented )_
- Generate new image version number based on the changeset types `major`, `minor`, `patch`
- build and tag the docker image
- push the docker image to the defined registry

If there are change sets, the pipeline will:

- Generate a new file in `.changes` directory with all the changes for this specific version
- (Re)generate the CHANGELOG.md file
- Bump the version number in `README.md` example and `pipe.yml` metadata
- Commit and push changes back to the repository
- Tag your commit with the new semantic version number

## BitBucket Prerequisites

You need to have a Keptn environment setup with an onboarded application with the SLI and SLO files registered. This pipe was tested with Keptn 0.6.0 quality-gate only installation.

# Development Process

## 1. Prerequisites

* Python & pip
* semversioner (tested with 0.1.4) -- `pip install semversioner`
* Mac or Unix environment. For the current helper script are written as UNIX shell scripts
* Dynatrace tenant with an API token
* Some monitored entity that can be identified with a Dynatrace TagMatchRule

## 2. Process to develop changes

1. Clone this repo
1. Make a new branch with the prefix of `feature-`
1. If have your own container registry, then adjust as required
1. Make changes to code
1. Test locally following instructions below

## 3. Local testing Testing

To run tests locally you need to:

1. Install [bats](https://opensource.com/article/19/2/testing-bash-bats) (bash test runner) using apt or any suitable package manager

    ```
    apt-get install bats
    ```

1. Make sure you've set up all required environment variables required for testing. Usually, these are the same variables that are required for a pipe to run.

1. You will need to have some time frame where you already processed a keptn SLO evaluation.  Use the Keptn Bridge to find the START and END times.

1. Get your Keptn URL and Token values from these [Keptn instructions](https://keptn.sh/docs/0.6.0/reference/cli/#keptn-auth)

1. Setup ENV variables that are unique to your setup and are secrets we dont want to check in.

    ```
    export KEPTN_URL=https://YOUR-KEPTN-DOMAIN
    export KEPTN_TOKEN=YOUR-KEPTN-TOKEN
    export START=2020-02-25T16:09:48Z
    export END=2020-02-26T19:41:24Z
    export TEST_PROJECT=bitbucket-demo
    export TEST_SERVICE=simplenodeservice
    export TEST_STAGE=development
    ```

1. Run bats from the root project folder
    ```
    bats test/test*
    ```
## 4. Testing from the bitbucket

You will need to:

1. uncomment the test step in `bitbucket-pipelines.yml`
1. You will need to have some time frame where you already processed a keptn SLO evaluation.  Use the Keptn Bridge to find the START and END times.
1. Get your Keptn URL and Token values from these [Keptn instructions](https://keptn.sh/docs/0.6.0/reference/cli/#keptn-auth)
1. Define the following repository variables to support the tests in `test/test.bat`

    * TEST_KEPTN_URL
    * TEST_KEPTN_TOKEN
    * TEST_START
    * TEST_END 
    * TEST_PROJECT
    * TEST_SERVICE
    * TEST_STAGE

## 4. Push the feature change to bitbucket 

1. Push changes to bitbucket and the pipeline will run.  This will build the image and push it to the registry
1. Test the newly create pipe in another pipeline to ensure it works

## 5. Bump the version

1. Once tested, call the `pushchange.sh` script that will call the semantic versioning script and git to push the changes back to bitbucket.  This in turn will run the pipeline
1. Once the BitBucket pipeline runs, then run `git pull` to bring back the updates change logs locally
1. Once finalized, make a Pull Request in BitBucket to merge the feature branch to master
1. Test the newly create pipe in another pipeline to ensure it works
1. Delete feature branch
