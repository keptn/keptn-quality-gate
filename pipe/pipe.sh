#!/usr/bin/env bash

source "$(dirname "$0")/common.sh"
DEBUG=${DEBUG:="false"}
PROCESS_STATUS=${PROCESS_STATUS:="ignore"}
enable_debug

info "Executing Pipe"

# Required parameters
KEPTN_URL=${KEPTN_URL:?'KEPTN_URL variable missing.'}
KEPTN_TOKEN=${KEPTN_TOKEN:?'KEPTN_TOKEN variable missing.'}
START=${START:?'START variable missing.'}
END=${END:?'END variable missing.'}
PROJECT=${PROJECT:?'PROJECT variable missing.'}
SERVICE=${SERVICE:?'SERVICE variable missing.'}
STAGE=${STAGE:?'STAGE variable missing.'}

# define the source value for the payload
if [ ! -z "${BITBUCKET_BUILD_NUMBER}" ]; then
  SOURCE="https://bitbucket.org/${BITBUCKET_REPO_FULL_NAME}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}"
else
  SOURCE=${SOURCE:="Local Testing"}
fi

info "================================================================="
info "Keptn Quality Gate:"
info ""
info "KEPTN_URL      = $KEPTN_URL"
info "START          = $START"
info "END            = $END"
info "PROJECT        = $PROJECT"
info "SERVICE        = $SERVICE"
info "STAGE          = $STAGE"
info "LABELS         = $LABELS"
info "SOURCE         = $SOURCE"
info "PROCESS_STATUS = $PROCESS_STATUS"
info "================================================================="

# build up POST_BODY variable
POST_BODY="{\"data\":{"
POST_BODY="${POST_BODY}\"start\":\"${START}\","
POST_BODY="${POST_BODY}\"end\":\"${END}\","
POST_BODY="${POST_BODY}\"project\":\"${PROJECT}\","
POST_BODY="${POST_BODY}\"service\":\"${SERVICE}\","
POST_BODY="${POST_BODY}\"stage\":\"${STAGE}\","
POST_BODY="${POST_BODY}\"teststrategy\":\"manual\""
[ ! -z "$LABELS" ] && POST_BODY="${POST_BODY},\"labels\":${LABELS}"
# add the closing bracket
POST_BODY="${POST_BODY}},"
POST_BODY="${POST_BODY}\"type\":\"sh.keptn.event.start-evaluation\","
POST_BODY="${POST_BODY}\"source\":\"${SOURCE}\"}"

if [[ "${DEBUG}" == "true" ]]; then
  info "KEPTN_URL = $KEPTN_URL"
  info "---"
  info "POST_BODY = $POST_BODY"
  info "---"
  ARGS+=( --verbose )
fi

info "Sending start Keptn Evaluation"
ctxid=$(curl -s -k -X POST --url "${KEPTN_URL}/v1/event" -H "Content-type: application/json" -H "x-token: ${KEPTN_TOKEN}" -d "$POST_BODY"|jq -r ".keptnContext")
debug "keptnContext ID = $ctxid"

loops=20
i=0
while [ $i -lt $loops ]
do
    i=`expr $i + 1`
    result=$(curl -s -k -X GET "${KEPTN_URL}/v1/event?keptnContext=${ctxid}&type=sh.keptn.events.evaluation-done" -H "accept: application/json" -H "x-token: ${KEPTN_TOKEN}")
    status=$(echo $result|jq -r ".data.evaluationdetails.result")
    if [ "$status" = "null" ]; then
      echo "Waiting for results (attempt $i of 20)..."
      sleep 10
    else
      break
    fi
done

filename=quality-gate-result.json
info "Writing results to file $filename."
echo $result|jq > $filename
cat $filename

info "================================================================="
info "eval status = ${status}"
info "eval result = $(echo $result|jq)"
info "================================================================="

# determine if process the result
if [ "${PROCESS_STATUS}" != "pass_on_warning" ] && [ "${PROCESS_STATUS}" != "fail_on_warning" ]; then
  exit
fi

if [ "$status" = "pass" ]; then
  success "Keptn Quality Gate - Evaluation Succeeded"
elif [ "$status" = "warning" ]; then
  if [ "${PROCESS_STATUS}" == "fail_on_warning" ]; then
    fail "Keptn Quality Gate - Got Warning status. Evaluation failed!"
    fail "For details visit the Keptn Bridge!"
    exit 1
  else
    success "Keptn Quality Gate - Evaluation Succeeded with Warning"
  fi
else
  fail "Keptn Quality Gate - Got Fail status. Evaluation failed!"
  fail "For details visit the Keptn Bridge!"
  exit 1
fi
