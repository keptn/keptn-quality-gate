#!/usr/bin/env bats

setup() {
  DEBUG=true
  DOCKER_IMAGE="keptn-test/keptn-quality-gate"
  docker build -t ${DOCKER_IMAGE} .
}

@test "test validate gate" {

  run docker run \
      -e KEPTN_URL="${TEST_KEPTN_URL}" \
      -e KEPTN_TOKEN="${TEST_KEPTN_TOKEN}" \
      -e START="${TEST_START}" \
      -e END="${TEST_END}" \
      -e PROJECT="${TEST_PROJECT}" \
      -e SERVICE="${TEST_SERVICE}" \
      -e STAGE="${TEST_STAGE}" \
      -e DEBUG="true" \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "test with missing argumment" {

  run docker run \
      -e KEPTN_URL="${TEST_KEPTN_URL}" \
      -e KEPTN_TOKEN="${TEST_KEPTN_TOKEN}" \
      -e PROJECT="a" \
      -e SERVICE="b" \
      -e STAGE="c" \
      -e DEBUG="true" \
      ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
}