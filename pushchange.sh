#!/usr/bin/env bash

# description of change. Shows in commit and release notes
CHANGE=$1
# branch name. eg master or feature-XXX
BRANCH=$2
# default to patch.  options are major, minor, patch
TYPE=$3

if [ -z "$CHANGE" ]; then
  echo "Missing change description"
  echo "Syntax: <change descripton> <branch name> <type: major, minor, patch>"
  exit 1
fi

if [ -z "$BRANCH" ]; then
  echo "Missing branch name"
  echo "Syntax: <change descripton> <branch name> <type: major, minor, patch>"
  exit 1
fi

if [ -z $TYPE ]; then
  TYPE=patch
fi

echo "Calling semversioner with TYPE = ${TYPE}"
semversioner add-change --type "${TYPE}" --description "${CHANGE}"
git add -A && git commit -m "${CHANGE}" && git push origin "${BRANCH}"